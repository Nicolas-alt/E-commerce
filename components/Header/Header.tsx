import { FC } from 'react'

const Header: FC = () => {
  return (
    <header>
      <nav>
        <li>Header</li>
      </nav>
    </header>
  )
}

export default Header
