export interface typesInterface {
  restorePassword: string
  recoverPassword: string
  updatePassword: string
  login: string
  logOut: string
  register: string
  tokenKey: string
}

export const authTypes: typesInterface = {
  restorePassword: '[AUTH]-[RESTORE-PASSWORD]',
  recoverPassword: '[AUTH]-[RECOVER-PASSWORD]',
  updatePassword: '[AUTH]-[UPDATE-PASSWORD]',
  login: '[AUTH]-[LOGIN]',
  logOut: '[AUTH]-[LOGOUT]',
  register: '[AUTH]-[REGISTER]',
  tokenKey: 'tmwToken'
}
