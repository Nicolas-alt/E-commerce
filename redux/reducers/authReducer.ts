import { authTypes, typesInterface } from '../types/authTypes'

interface payLoadAuthInterface {
  isLoggedIn: boolean
}

interface actionAuthInterface {
  type: typesInterface | any
  payload: payLoadAuthInterface
}

const initialState: payLoadAuthInterface = {
  isLoggedIn: false
}

export const authReducer = (
  state = initialState,
  action: actionAuthInterface
) => {
  switch (action.type) {
    case authTypes.login:
      return {
        ...state,
        isLoggedIn: true
      }

    case authTypes.logOut:
      return {
        ...initialState
      }

    case authTypes.register:
      return {
        ...state,
        isLoggedIn: true
      }

    case authTypes.updatePassword:
      return {
        ...state
      }

    case authTypes.recoverPassword:
      return {
        ...state,
        ...action.payload
      }

    default:
      return state
  }
}
