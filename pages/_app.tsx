import 'tailwindcss/tailwind.css'
import { Provider } from 'react-redux'
import { Toaster } from 'react-hot-toast'
import { store } from '../redux/store'

function MyApp({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <Component {...pageProps} />
      <Toaster />
    </Provider>
  )
}

export default MyApp
