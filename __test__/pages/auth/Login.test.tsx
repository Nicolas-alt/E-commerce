import React from 'react'
import { screen, render } from '@testing-library/react'
import Login from '../../../pages/auth/login'

describe('Login page', () => {
  test('Should have "Register" text', () => {
    render(<Login />)
    const wrapper = screen.getByText(/SingIn/i)
    expect(wrapper).toBeInTheDocument()
  })

  test('Should take a snapshot', () => {
    const wrapper = render(<Login />)
    expect(wrapper).toMatchSnapshot()
  })
})
