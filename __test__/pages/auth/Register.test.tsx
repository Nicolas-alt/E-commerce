import React from 'react'
import { screen, render } from '@testing-library/react'
import Register from '../../../pages/auth/register'

describe('Register page', () => {
  test('Should have "Register" text', () => {
    render(<Register />)
    const wrapper = screen.getByText(/SingUP/i)
    expect(wrapper).toBeInTheDocument()
  })

  test('Should take a snapshot', () => {
    const wrapper = render(<Register />)
    expect(wrapper).toMatchSnapshot()
  })
})
