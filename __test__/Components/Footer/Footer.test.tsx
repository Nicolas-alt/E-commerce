import React from 'react'
import { screen, render } from '@testing-library/react'
import Footer from '@/components/Footer/Footer'

describe('<Footer />', () => {
  test('Should have "Footer" text', () => {
    render(<Footer />)
    const wrapper = screen.getByText(/Footer/i)
    expect(wrapper).toBeInTheDocument()
  })

  test('Should take a snapshot', () => {
    const wrapper = render(<Footer />)
    expect(wrapper).toMatchSnapshot()
  })
})
