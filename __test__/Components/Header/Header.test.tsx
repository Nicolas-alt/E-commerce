import React from 'react'
import { render, screen } from '@testing-library/react'
import Header from '@/components/Header/Header'

describe('<Header />', () => {
  test('Should render "Header" text', () => {
    render(<Header />)

    const wrapper = screen.getByText(/Header/i)
    expect(wrapper).toBeInTheDocument()
  })

  test('Should take a snapshot', () => {
    const wrapper = render(<Header />)
    expect(wrapper).toMatchSnapshot()
  })
})
